/**
 * 
 */
package no.ntnu.imt3281.movieExplorer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 */
public class JSON {
	
	ArrayList<Pair> pairs;
	
	/**
	 * Parses json
	 * @param jsoninput 
	 */
	public JSON(String jsoninput) {
		JSONParser parser = new JSONParser();
		pairs = new ArrayList<Pair>();
		
		try {
			JSONObject object = (JSONObject)parser.parse(jsoninput);
			
			parseJSONObject(object);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
	}


	/**
	 * Initializes a new JSON object using a JSONArray
	 * as key values
	 * 
	 * @param array JSONArray
	 */
	public JSON(JSONArray array) {
		pairs = new ArrayList<Pair>();
		
		Iterator<Object> iterator = array.iterator();
		int i = 0;
		
		while(iterator.hasNext())
		{
			++i;
			
			Object object = iterator.next();
			
			if(object instanceof JSONArray)
			{
				pairs.add(new Pair(Integer.toString(i), new JSON((JSONArray)object)));
			}
			else if(object instanceof JSONObject)
			{
				pairs.add(new Pair(Integer.toString(i) , new JSON((JSONObject)object)));				
			}
			else
			{
				pairs.add(new Pair(Integer.toString(i), object));
			}
		}
	}
	
	public JSON(JSONObject object)
	{
		pairs = new ArrayList<Pair>();
		
		parseJSONObject(object);
	}

	
	/**
	 * Parses a JSONObject
	 * 
	 * @param object Object to parse
	 */
	private void parseJSONObject(JSONObject object)
	{
		Set<String> keys = object.keySet();
		
		for(String key : keys)
		{
			Object value = object.get(key);
			
			if(value instanceof JSONArray)
			{
				pairs.add(new Pair(key, new JSON((JSONArray)value)));
			}
			else if(value instanceof JSONObject)
			{
				pairs.add(new Pair(key, new JSON((JSONObject)value)));
			}
			else
			{
				pairs.add(new Pair(key, value));
			}
		}
	}

	/**
	 * Returns the value of a key
	 * 
	 * @param key
	 * @return The value as object
	 */
	public Object getValue(String key) {
		for(int i = 0; i < pairs.size(); ++i)
		{
			if(pairs.get(i).equals(key))
			{
				return pairs.get(i).getValue();
			}
		}
		
		return null;
	}

	/**
	 * Returns the value of a index
	 * @param index The index
	 * @return The value as object
	 */
	public Object getValue(int index)
	{
		return pairs.get(index).getValue();
	}
	
	/**
	 * Returns the object with this key
	 * 
	 * @param key The key
	 * @return value
	 */
	public JSON get(String key) {
		for(int i = 0; i < pairs.size(); ++i)
		{
			if(pairs.get(i).equals(key))
			{
				return (JSON)pairs.get(i).getValue();
			}
		}
		return null;
	}
	
	/**
	 * A function to add a key into a JSON object
	 * @param key Key to add, always a string
	 * @param Object can be any basic variable or another JSON object
	 */
	public void addPair(String key, Object object)
	{
		pairs.add(new Pair(key, object));
	}
	
	/**
	 * Gets the object at index i
	 * 
	 * @param i Index
	 * @return JSON object
	 */
	public JSON get(int i)
	{
		return (JSON)pairs.get(i).getValue();
	}

	
	/**
	 * Size of the JSON's array of pairs
	 * @return size of the array
	 */
	public int size() {
		return pairs.size();
	}	
	
	/**
	 * Helper class for keeping keys and values together
	 */
	private class Pair
	{
		private final String key;
		private final Object value;
		
		public Pair(String key, Object value)
		{
			this.key = key;
			this.value = value;
		}
		
		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj)
		{
			if(obj instanceof String && key.equals(obj))
			{
				return true;
			}
			
			return false;
		}
		
		/**
		 * Returns the value of the pair
		 * 
		 * @return value
		 */
		public Object getValue()
		{
			return value;
		}
	}
}

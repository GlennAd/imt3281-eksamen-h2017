/**
 * 
 */
package no.ntnu.imt3281.movieExplorer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

/**
 * Class to get URL for the highest resolution URL for
 * a given picture name
 */
public class TheMovieDBConfiguration {
	/**
	 * Data is stored in JSON form
	 */
	private JSON data;
	
	/**
	 * To store preferences
	 */
	private static Preferences prefs;
	
	/**
	 * Constructs a new config with new image data
	 * @param json JSON string containing config info
	 */
	public TheMovieDBConfiguration(String json) {
		data = new JSON(json);
		prefs = Preferences.userRoot().node(TheMovieDBConfiguration.class.getName());
	}

	/**
	 * Returns highest resolution URL for picture
	 * as backdrop
	 * @param picture Picture name
	 * @return url to picture
	 */
	public String getBackdropURL(String picture) {
		JSON temp = data.get("images").get("backdrop_sizes");
		
		if(prefs.get("storeLocation", "") != "")
		{
			File f = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + picture);
			if(!f.exists())
			{
				Image image = new Image(appendSharedURL((String)temp.getValue(temp.size() - 2), picture));
				File storeLocation = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + "/");
				
				BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
				
				try {
					ImageIO.write(bImage, ".jpg", storeLocation);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			return "file:" + f.getAbsolutePath();
		}
		
		return appendSharedURL((String)temp.getValue(temp.size() - 2), picture);
	}

	/**
	 * Returns highest resolution URL for picture
	 * as logo
	 * @param picture Picture name
	 * @return url to picture
	 */
	public String getLogoURL(String picture) {
		JSON temp = data.get("images").get("logo_sizes");
		
		if(prefs.get("storeLocation", "") != "")
		{
			File f = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + picture);
			if(!f.exists())
			{
				Image image = new Image(appendSharedURL((String)temp.getValue(temp.size() - 2), picture));
				File storeLocation = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + "/");
				
				BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
				
				try {
					ImageIO.write(bImage, ".jpg", storeLocation);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			return "file:" + f.getAbsolutePath();
		}
		
		return appendSharedURL((String)temp.getValue(temp.size() - 2), picture);
	}

	/**
	 * Returns highest resolution URL for picture
	 * as poster
	 * @param picture Picture name
	 * @return url to picture
	 */
	public String getPosterURL(String picture) {
		JSON temp = data.get("images").get("poster_sizes");
		
		if(prefs.get("storeLocation", "") != "")
		{
			File f = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + picture);
			if(!f.exists())
			{
				Image image = new Image(appendSharedURL((String)temp.getValue(temp.size() - 2), picture));
				File storeLocation = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + "\\");
				
				BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
				
				try {
					ImageIO.write(bImage, ".jpg", storeLocation);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			return "file:" + f.getAbsolutePath();
		}
		
		return appendSharedURL((String)temp.getValue(temp.size() - 2), picture);
	}

	/**
	 * Returns highest resolution URL for picture
	 * as profile
	 * @param picture Picture name
	 * @return url to picture
	 */
	public String getProfileURL(String picture) {
		JSON temp = data.get("images").get("profile_sizes");
		
		if(prefs.get("storeLocation", "") != "")
		{
			File f = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + picture);
			if(!f.exists())
			{
				Image image = new Image(appendSharedURL((String)temp.getValue(temp.size() - 2), picture));
				File storeLocation = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + "/");
				
				BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
				
				try {
					ImageIO.write(bImage, ".jpg", storeLocation);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			return "file:" + f.getAbsolutePath();
		}
		
		return appendSharedURL((String)temp.getValue(temp.size() - 2), picture);
	}

	/**
	 * Returns highest resolution URL for picture
	 * as still
	 * @param picture Picture name
	 * @return url to picture
	 */
	public String getStillURL(String picture) {
		JSON temp = data.get("images").get("still_sizes");
		
		if(prefs.get("storeLocation", "") != "")
		{
			File f = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + picture);
			if(!f.exists())
			{
				Image image = new Image(appendSharedURL((String)temp.getValue(temp.size() - 2), picture));
				File storeLocation = new File(prefs.get("storeLocation", "") + (String)temp.getValue(temp.size() - 2) + "/");
				
				BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
				
				try {
					ImageIO.write(bImage, ".jpg", storeLocation);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			return "file:" + f.getAbsolutePath();
		}
		
		return appendSharedURL((String)temp.getValue(temp.size() - 2), picture);
	}
	
	/**
	 * Helper function for appending the shared parts of this picture
	 * 
	 * @param format The format they want the picture in
	 * @param picture The picture name
	 * @return Url to the picture
	 */
	private String appendSharedURL(String format, String picture)
	{
		StringBuilder string = new StringBuilder((String)data.get("images").getValue("base_url"));
		
		string.append(format);
		
		string.append("/");
		string.append(picture);
		
		return string.toString();
	}
	
	/**
	 * Sets the directory location in preferences
	 * @param path Directory to store images in
	 */
	public static void setPrefs(String path)
	{
		prefs = Preferences.userRoot().node(TheMovieDBConfiguration.class.getName());
		prefs.put("storeLocation", path + "\\");
	}
}

package no.ntnu.imt3281.movieExplorer;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class GUI {
    @FXML private TextField searchField;
    @FXML private TreeView<SearchResultItem> searchResult;
    @FXML private Pane detailPane;
    
    // Root node for search result tree view
    private TreeItem<SearchResultItem> searchResultRootNode = new TreeItem<SearchResultItem> (new SearchResultItem(""));
    
    @FXML
    /**
     * Called when the object has been created and connected to the fxml file. All components defined in the fxml file is 
     * ready and available.
     */
    public void initialize() {
    		searchResult.setRoot(searchResultRootNode);
    }

    @FXML
    /**
     * Called when the seqrch button is pressed or enter is pressed in the searchField.
     * Perform a multiSearch using theMovieDB and add the results to the searchResult tree view.
     * 
     * @param event ignored
     */
    void search(ActionEvent event) {
    		JSON result = Search.multiSearch(searchField.getText()).get("results");
    		TreeItem<SearchResultItem> searchResults = new TreeItem<> (new SearchResultItem("Searching for : "+searchField.getText()));
    		searchResultRootNode.getChildren().add(searchResults);
    		for (int i=0; i<result.size(); i++) {
    			SearchResultItem item = new SearchResultItem(result.get(i));
    			searchResults.getChildren().add(new TreeItem<SearchResultItem>(item));
    		}
    		searchResultRootNode.setExpanded(true);
    		searchResults.setExpanded(true);
    }
    
    @FXML
    /**
     * Called when the user clicks somewhere on the treeview.
     * Handles the click
     * @param e The event is ignored (had some fx problems on my side so I had to use the full path)
     */
    void handleClick(javafx.scene.input.MouseEvent e)
    {
    	TreeItem<SearchResultItem> currentlySelected = searchResult.getSelectionModel().getSelectedItem();
    	
    	if(currentlySelected != null && currentlySelected.getChildren().size() == 0)
    	{
    		if(currentlySelected.getValue().media_type.equals("person"))
    		{
	    		JSON result = Search.takesPartIn(currentlySelected.getValue().id);
	        	
	        	for(int i = 0; i < result.size(); ++i)
	        	{
	        		JSON object = result.get("results").get(i);
	        		object.addPair("media_type", "movie");
	        		SearchResultItem item = new SearchResultItem(object);
	        		currentlySelected.getChildren().add(new TreeItem<SearchResultItem>(item));
	        	}		
    		}
    		else if(currentlySelected.getValue().media_type.equals("movie"))
    		{
    			JSON result = Search.actors(currentlySelected.getValue().id);
    			
    			try {
    				FXMLLoader loader = new FXMLLoader(getClass().getResource("movieDetails.fxml"));
    				
					AnchorPane movieDetails = (AnchorPane)loader.load();
					movieDetailsController controller = loader.<movieDetailsController>getController();
					controller.newMovie(currentlySelected.getValue().id);
					
					if(!detailPane.getChildren().isEmpty())
					{
						detailPane.getChildren().remove(0);
					}
					detailPane.getChildren().add(movieDetails);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
    			
    			for(int i = 0; i < result.size(); ++i)
    			{
    				JSON object = result.get("cast").get(i);
    				object.addPair("media_type", "person");
    				SearchResultItem item = new SearchResultItem(object);
    				currentlySelected.getChildren().add(new TreeItem<SearchResultItem>(item));
    			}
    		}
    	}
    }
    
    @FXML
    /**
     * Sets the preferenses
     */
    public void preferanses(ActionEvent e)
    {
    	Stage stage =  (Stage) detailPane.getScene().getWindow();
    	
    	DirectoryChooser chooser = new DirectoryChooser();
    	chooser.setTitle("Choose directory to cache images");
    	
    	File selectedDirectory = chooser.showDialog(stage);
    	
    	if(selectedDirectory != null)
    	{
    		try {
				Files.createDirectory(selectedDirectory.toPath().resolve("w1280"));
				Files.createDirectory(selectedDirectory.toPath().resolve("w500"));
				Files.createDirectory(selectedDirectory.toPath().resolve("w780"));
				Files.createDirectory(selectedDirectory.toPath().resolve("h623"));
				Files.createDirectory(selectedDirectory.toPath().resolve("w300"));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
    		TheMovieDBConfiguration.setPrefs(selectedDirectory.getAbsolutePath());
    	}
    }
    class SearchResultItem {
    		private String media_type = "";
    		private String name = "";
    		private long id;
    		private String profile_path = "";
    		private String title = ""; 
    		
    		/**
    		 * Create new SearchResultItem with the given name as what will be displayed in the tree view.
    		 * 
    		 * @param name the value that will be displayed in the tree view
    		 */
    		public SearchResultItem(String name) {
    			this.name = name;
    		}
    		
    		/**
    		 * Create a new SearchResultItem with data form this JSON object.
    		 * 
    		 * @param json contains the data that will be used to initialize this object.
    		 */
		public SearchResultItem(JSON json) {
			media_type = (String) json.getValue("media_type");
			if (media_type.equals("person")) {
				name = (String)json.getValue("name");	
				profile_path = (String)json.getValue("profile_path");
				id = (long)json.getValue("id");
			} else if (media_type.equals("movie")) {
				title = (String)json.getValue("title");
				id = (long)json.getValue("id");
			} else if(media_type.equals("tv")) {
				title = (String)json.getValue("name");
			}
			id = (Long)json.getValue("id");
		}
    		
		/**
		 * Used by the tree view to get the value to display to the user. 
		 */
		@Override
		public String toString() {
			if (media_type.equals("person")) {
				return name;
			} else if (media_type.equals("movie")) {
				return title;
			} else if (media_type.equals("tv")) {
				return title;
			} else {
				return name;
			}
		}
    }
}

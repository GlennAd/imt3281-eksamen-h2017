package no.ntnu.imt3281.movieExplorer;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * Controller for the movieDetails.fxml
 */
public class movieDetailsController {
	/**
	 * Stored string for getting picture sizes/base_url
	 */
	private static String imageString = String.join("", "{\n",
			"    \"images\": {\n",
			"        \"base_url\": \"http://image.tmdb.org/t/p/\",\n",
			"        \"secure_base_url\": \"https://image.tmdb.org/t/p/\",\n",
			"        \"backdrop_sizes\": [\n",
			"            \"w300\",\n",
			"            \"w780\",\n",
			"            \"w1280\",\n",
			"            \"original\"\n",
			"        ],\n",
			"        \"logo_sizes\": [\n",
			"            \"w45\",\n",
			"            \"w92\",\n",
			"            \"w154\",\n",
			"            \"w185\",\n",
			"            \"w300\",\n",
			"            \"w500\",\n",
			"            \"original\"\n",
			"        ],\n",
			"        \"poster_sizes\": [\n",
			"            \"w92\",\n",
			"            \"w154\",\n",
			"            \"w185\",\n",
			"            \"w342\",\n",
			"            \"w500\",\n",
			"            \"w780\",\n",
			"            \"original\"\n",
			"        ],\n",
			"        \"profile_sizes\": [\n",
			"            \"w45\",\n",
			"            \"w185\",\n",
			"            \"h632\",\n",
			"            \"original\"\n",
			"        ],\n",
			"        \"still_sizes\": [\n",
			"            \"w92\",\n",
			"            \"w185\",\n",
			"            \"w300\",\n",
			"            \"original\"\n",
			"        ]\n",
			"    },\n",
			"    \"change_keys\": [\n",
			"        \"adult\",\n",
			"        \"air_date\",\n",
			"        \"also_known_as\",\n",
			"        \"alternative_titles\",\n",
			"        \"biography\",\n",
			"        \"birthday\",\n",
			"        \"budget\",\n",
			"        \"cast\",\n",
			"        \"certifications\",\n",
			"        \"character_names\",\n",
			"        \"created_by\",\n",
			"        \"crew\",\n",
			"        \"deathday\",\n",
			"        \"episode\",\n",
			"        \"episode_number\",\n",
			"        \"episode_run_time\",\n",
			"        \"freebase_id\",\n",
			"        \"freebase_mid\",\n",
			"        \"general\",\n",
			"        \"genres\",\n",
			"        \"guest_stars\",\n",
			"        \"homepage\",\n",
			"        \"images\",\n",
			"        \"imdb_id\",\n",
			"        \"languages\",\n",
			"        \"name\",\n",
			"        \"network\",\n",
			"        \"origin_country\",\n",
			"        \"original_name\",\n",
			"        \"original_title\",\n",
			"        \"overview\",\n",
			"        \"parts\",\n",
			"        \"place_of_birth\",\n",
			"        \"plot_keywords\",\n",
			"        \"production_code\",\n",
			"        \"production_companies\",\n",
			"        \"production_countries\",\n",
			"        \"releases\",\n",
			"        \"revenue\",\n",
			"        \"runtime\",\n",
			"        \"season\",\n",
			"        \"season_number\",\n",
			"        \"season_regular\",\n",
			"        \"spoken_languages\",\n",
			"        \"status\",\n",
			"        \"tagline\",\n",
			"        \"title\",\n",
			"        \"translations\",\n",
			"        \"tvdb_id\",\n",
			"        \"tvrage_id\",\n",
			"        \"type\",\n",
			"        \"video\",\n",
			"        \"videos\"\n",
			"    ]\n",
			"}");
	
	@FXML
	/**
	 * Profile image of movie
	 */
	private ImageView image;

	@FXML
	/**
	 * tl:dr of move
	 */
	private TextArea overview;
	
	@FXML
	/**
	 * Title of movie
	 */
	private Label title;
	
	@FXML
	/**
	 * List of genres the movie covers
	 */
	private VBox genreList;
	
	/**
	 * Updates the panel with a new movie
	 * 
	 * @param id Movie id
	 */
	public void newMovie(long id)
	{
		JSON json = Search.movie(id);
		TheMovieDBConfiguration configuration = new TheMovieDBConfiguration(imageString);
		
		
		title.setText((String)json.getValue("title"));
		overview.setText((String)json.getValue("overview"));
		image.setImage(new Image(configuration.getPosterURL((String)json.getValue("poster_path"))));
		
		for(int i = 0; i < json.get("genres").size(); ++i)
		{
			genreList.getChildren().add(new Label((String)json.get("genres").get(i).getValue("name")));
		}
	}
}

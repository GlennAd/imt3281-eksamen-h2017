package no.ntnu.imt3281.movieExplorer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Database class to deal with creating
 * and storing/reading things from a database
 */
public class Database {
	private static final String URL = "jdbc:derby:MovieDB";
	private static Connection con;
	
	/**
	 * Sets up a connection to the database, will also create a new one if it
	 * dosen't already exist
	 */
	public static void setUpConnection() {
		try {
            con = DriverManager.getConnection(URL);
		} catch (SQLException sqle) {		// No database exists
			try {							// Try creating database
				con = DriverManager.getConnection(URL+";create=true");
				setupDB();
			} catch (SQLException sqle1) {	// Unable to create database, exit server
				System.err.println("Unable to create database"+sqle1.getMessage());
				System.exit(-1);
			}
		}
	}
	
	/**
	 * Setup a fresh database
	 * @throws SQLException
	 */
	private static void setupDB() throws SQLException {
		Statement stmt = con.createStatement();
		stmt.execute("CREATE TABLE genres (id bigint NOT NULL, "
                + "name varchar(128) NOT NULL, "
                + "PRIMARY KEY  (id))");
		
		stmt.execute("CREATE TABLE multi (id varchar(128) NOT NULL, "
					+ "query LONG VARCHAR, "
					+ "PRIMARY KEY (id))");
		
		stmt.execute("CREATE TABLE actors (id bigint NOT NULL, "
				+ "query LONG VARCHAR, "
				+ "PRIMARY KEY (id))");
		
		stmt.execute("CREATE TABLE takePartIn (id bigint NOT NULL, "
				+ "query LONG VARCHAR, "
				+ "PRIMARY KEY (id))");
		
		
	}
	
	/**
	 * Function to select things from the database
	 * @param statement Select statement to process
	 */
	public static ResultSet query(String statement)
	{
		try {
			Statement stmt = con.createStatement();
			
			ResultSet results = stmt.executeQuery(statement);
			
			return results;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	/**
	 * Function for insert statements
	 * @param statement The Insert statement
	 * @return number of inserts
	 */
	public static int insert(String statement)
	{
		int count = 0;
		
		try {
			Statement stmt = con.createStatement();
			
			count = stmt.executeUpdate(statement);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}
}

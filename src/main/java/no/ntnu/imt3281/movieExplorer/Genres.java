/**
 * 
 */
package no.ntnu.imt3281.movieExplorer;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Static class to convert genre ID's to names
 *
 */
public class Genres {
	private static JSON genres = null;
	
	/**
	 * Converts and id to name
	 * @param id Id to convert
	 * @return Genres name
	 */
	public static String resolve(int id) {
		Database.setUpConnection();
		
		ResultSet results = Database.query("SELECT name "
												+ "FROM genres "
												+ "WHERE id = " + Integer.toString(id));
		
		try {
			if(results.next())
			{
				return results.getString(1);		
			}
			else
			{
				if(genres == null)
				{
					genres = Search.genres();
				}
				
				for(int i = 0; i < genres.get("genres").size(); ++i)
				{
					if((long)genres.get("genres").get(i).getValue("id") == id)
					{
						String name = (String)genres.get("genres").get(i).getValue("name");
						
						Database.insert("INSERT INTO genres "
										+ "VALUES (" + Integer.toString(id) + ", '" + name + "')");
						
						return name;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}

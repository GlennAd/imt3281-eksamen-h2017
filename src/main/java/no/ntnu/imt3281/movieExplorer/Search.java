/**
 * 
 */
package no.ntnu.imt3281.movieExplorer;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Search class used to get info from themoviedb
 */
public class Search {

	/**
	 * Multisearch feature
	 * 
	 * @param query Query for the multisearch
	 * @return JSON object created from the query result
	 */
	public static JSON multiSearch(String query) {
		
		try {
			Database.setUpConnection();
			
			
			ResultSet set = Database.query("SELECT query "
											+ "FROM multi "
											+ "WHERE id = '" + query.replaceAll(" ", "") + "'");
			
			try {
				if(set.next())
				{
					return new JSON(set.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			String req = "https://api.themoviedb.org/3/search/multi?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US&query=";
			req += query.replaceAll(" ", "%20");
			req += "&page=1&include_adult=false";
					
			req = Unirest.get(req).asString().getBody();
			
			return new JSON(req);
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Search for actors by id
	 * 
	 * @param id Movie's ID
	 * @return JSON object created from the query
	 */
	public static JSON actors(long id) {
		Database.setUpConnection();
		
		ResultSet set = Database.query("SELECT query "
				+ "FROM actors "
				+ "WHERE id = " + Long.toString(id));

		try {
			if(set.next())
			{
				return new JSON(set.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		String req = "https://api.themoviedb.org/3/movie/"
				+ Long.toString(id)
				+ "/credits?api_key=a47f70eb03a70790f5dd711f4caea42d";
		try {
			req = Unirest.get(req).asString().getBody();
			return new JSON(req);
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Searches for movies where a actor has taken part in
	 * 
	 * @param id Actor's id
	 * @return JSON object created from query result
	 */
	public static JSON takesPartIn(long id) {
		Database.setUpConnection();
		
		ResultSet set = Database.query("SELECT query "
				+ "FROM takePartIn "
				+ "WHERE id = " + Long.toString(id));

		try {
			if(set.next())
			{
				return new JSON(set.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		String req = "https://api.themoviedb.org/3/discover/movie?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_people="
				+ Long.toString(id);
		
		try {
			req = Unirest.get(req).asString().getBody();
			return new JSON(req);
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Searches for movie credits
	 * 
	 * @param id Movie id
	 * @return JSON object containing query result
	 */
	public static JSON movie(long id) {
		Database.setUpConnection();
		
		String req =  "https://api.themoviedb.org/3/movie/ " + Long.toString(id) + "?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US";
		
		try {
			req = Unirest.get(req).asString().getBody();
			return new JSON(req);
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * Searches for all genres
	 * @return JSON containing all genres for movies and tv
	 */
	public static JSON genres()
	{
		String req = "https://api.themoviedb.org/3/genre/movie/list?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US";
		String req2 = "https://api.themoviedb.org/3/genre/tv/list?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US";
		
		try {
			req = Unirest.get(req).asString().getBody();
			req2 = Unirest.get(req2).asString().getBody();
			
			req = req.substring(0, req.lastIndexOf(']'));
			req2 = req2.substring(req2.indexOf('[') + 1, req2.length());
			
			return new JSON(req + "," + req2);
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
